// import 'dart:html';
import 'dart:math';
//sldkfjdsk
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:neomorphismapp/pages/second/second.dart';
import 'package:neomorphismapp/pages/home/home.dart';

class Calculator extends StatefulWidget {
  const Calculator({super.key});

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  String mathExpression = "";
  String result = "";
  IconData themModeIcon = Icons.sunny;

  Widget calcButton(String s, bool isColorful) {
    return NeumorphicButton(
      onPressed: () {
        onButtonClicked(s);
      },
      style: NeumorphicStyle(
        color: NeumorphicTheme.baseColor(context),
        shape: NeumorphicShape.flat,
        boxShape: NeumorphicBoxShape.circle(),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width / 8,
        height: MediaQuery.of(context).size.width / 8,
        child: Center(
          child: Text(
            s,
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: isColorful
                    ? NeumorphicTheme.accentColor(context)
                    : NeumorphicTheme.isUsingDark(context)
                        ? Colors.white
                        : Colors.black),
          ),
        ),
      ),
    );
  }

  Widget buttons() {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Container(
        height: MediaQuery.of(context).size.height / 2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                calcButton("CL", true),
                calcButton("DEL", true),
                calcButton("^", true),
                calcButton("*", true)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                calcButton("1", false),
                calcButton("2", false),
                calcButton("3", false),
                calcButton("/", true)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                calcButton("4", false),
                calcButton("5", false),
                calcButton("6", false),
                calcButton("+", true)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                calcButton("7", false),
                calcButton("8", false),
                calcButton("9", false),
                calcButton("-", true)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                calcButton("0", false),
                calcButton("00", false),
                calcButton(".", false),
                calcButton("=", true)
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget mathExpressionCreator() {
    List<Widget> list = [];
    for (var i = 0; i < mathExpression.length; i++) {
      String s = mathExpression[i];
      if (["^", "*", "/", "+", "-"].contains(s)) {
        list.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: 5),
          child: Text(s,
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: NeumorphicTheme.accentColor(context))),
        ));
      } else {
        list.add(Text(
          s,
          style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              color: NeumorphicTheme.isUsingDark(context)
                  ? Colors.white
                  : Colors.black),
        ));
      }
    }
    return Wrap(
      children: list,
    );
  }

  Widget resultCreator() {
    return result == ""
        ? Container()
        : Row(
            children: [
              result[0] == "-"
                  ? Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Text("-",
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: NeumorphicTheme.accentColor(context))),
                    )
                  : Container(),
              Text(
                result[0] == "-" ? result.substring(1) : result,
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: NeumorphicTheme.isUsingDark(context)
                        ? Colors.white
                        : Colors.black),
              ),
              Text("=",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: NeumorphicTheme.accentColor(context)))
            ],
          );
  }

  void onButtonClicked(String s) {
    setState(() {
      if (s == "CL") {
        mathExpression = "";
        result = "";
      } else if (s == "DEL") {
        mathExpression = mathExpression == ""
            ? ""
            : mathExpression.substring(0, mathExpression.length - 1);
        result = "";
      } else if (s == "^") {
        if (result == "") {
          mathExpression = mathExpression != "" &&
                  !["^", "*", "/", "+", "-", "."]
                      .contains(mathExpression[mathExpression.length - 1])
              ? "${mathExpression}${s}"
              : mathExpression;
        } else {
          mathExpression = "${result}${s}";
          result = "";
        }
      } else if (s == "*") {
        if (result == "") {
          mathExpression = mathExpression != "" &&
                  !["^", "*", "/", "+", "-", "."]
                      .contains(mathExpression[mathExpression.length - 1])
              ? "${mathExpression}${s}"
              : mathExpression;
        } else {
          mathExpression = "${result}${s}";
          result = "";
        }
      } else if (s == "/") {
        if (result == "") {
          mathExpression = mathExpression != "" &&
                  !["^", "*", "/", "+", "-", "."]
                      .contains(mathExpression[mathExpression.length - 1])
              ? "${mathExpression}${s}"
              : mathExpression;
        } else {
          mathExpression = "${result}${s}";
          result = "";
        }
      } else if (s == "+") {
        if (result == "") {
          mathExpression = mathExpression != "" &&
                  !["^", "*", "/", "+", "-", "."]
                      .contains(mathExpression[mathExpression.length - 1])
              ? "${mathExpression}${s}"
              : mathExpression;
        } else {
          mathExpression = "${result}${s}";
          result = "";
        }
      } else if (s == "-") {
        if (result == "") {
          mathExpression = mathExpression == "" ||
                  (mathExpression != "" &&
                      ![".", "+", "-"]
                          .contains(mathExpression[mathExpression.length - 1]))
              ? "${mathExpression}${s}"
              : mathExpression;
        } else {
          mathExpression = "${result}${s}";
          result = "";
        }
      } else if (s == "=") {
        //todo : we should write the logic for result value!
        if (mathExpression != "" &&
            !["^", "*", "/", "+", "-", "."]
                .contains(mathExpression[mathExpression.length - 1])) {
          result = calculateResult().toStringAsFixed(3);
        }
      } else if (s == ".") {
        //todo : we should check for existing dot before this dart!
        mathExpression = mathExpression != "" &&
                !["^", "*", "/", "+", "-", "."]
                    .contains(mathExpression[mathExpression.length - 1])
            ? "${mathExpression}${s}"
            : mathExpression;
        result = "";
      } else if (s == "00") {
        mathExpression = mathExpression != "" &&
                ["1", "2", "3", "4", "5", "6", "7", "8", "9", "."]
                    .contains(mathExpression[mathExpression.length - 1])
            ? "${mathExpression}${s}"
            : mathExpression;
        result = "";
      } else if (s == "0") {
        mathExpression = "${mathExpression}${s}";
        result = "";
      } else {
        mathExpression = "${mathExpression}${s}";
        result = "";
      }
    });
  }

  double calculateResult() {
    List<String> operators = [];
    List<double> operands = [];
    int lastOperatorIndex = -1;

    //detecting operators and operands:
    for (var i = 0; i < mathExpression.length; i++) {
      if (["^", "*", "/", "+"].contains(mathExpression[i])) {
        operators.add(mathExpression[i]);
        print("operator detected : ${mathExpression[i]}");
        operands.add(
            double.parse(mathExpression.substring(lastOperatorIndex + 1, i)));
        lastOperatorIndex = i;
      } else if (mathExpression[i] == "-" &&
          i != 0 &&
          !["^", "*", "/"].contains(mathExpression[i - 1])) {
        operators.add(mathExpression[i]);
        operands.add(
            double.parse(mathExpression.substring(lastOperatorIndex + 1, i)));
        lastOperatorIndex = i;
      }
    }
    operands.add(double.parse(mathExpression.substring(
        lastOperatorIndex + 1, mathExpression.length)));

    //calculate math expression:
    List<String> operatorsListOrderedByPriority = ["^^", "*/", "+-"];
    int operatorsListOrderedByPriorityIndexCounter = 0;
    while (operators.isNotEmpty) {
      int foundedIndex1 = operators.indexOf(operatorsListOrderedByPriority[
          operatorsListOrderedByPriorityIndexCounter][0]);
      int foundedIndex2 = operators.indexOf(operatorsListOrderedByPriority[
          operatorsListOrderedByPriorityIndexCounter][1]);
      int foundedIndex = -1;
      if ((foundedIndex1 <= foundedIndex2 && foundedIndex1 >= 0) ||
          foundedIndex2 < 0) {
        foundedIndex = foundedIndex1;
      } else if ((foundedIndex2 <= foundedIndex1 && foundedIndex2 >= 0) ||
          foundedIndex1 < 0) {
        foundedIndex = foundedIndex2;
      }

      if (foundedIndex == -1) {
        operatorsListOrderedByPriorityIndexCounter++;
        continue;
      } else {
        double res = 0;
        double operand1 = operands[foundedIndex];
        double operand2 = operands[foundedIndex + 1];
        if (operators[foundedIndex] == "^") {
          res = (operand1 < 0 ? -1 : 1) *
              pow(operand1 < 0 ? (-1) * operand1 : operand1, operand2)
                  .toDouble();
        } else if (operators[foundedIndex] == "*") {
          res = operand1 * operand2;
        } else if (operators[foundedIndex] == "/") {
          res = operand1 / operand2;
        } else if (operators[foundedIndex] == "+") {
          res = operand1 + operand2;
        } else if (operators[foundedIndex] == "-") {
          res = operand1 - operand2;
        }
        operators.removeAt(foundedIndex);
        operands.removeRange(foundedIndex, foundedIndex + 2);
        operands.insert(foundedIndex, res);
      }
    }
    return operands[0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NeumorphicAppBar(
        centerTitle: true,
        title: NeumorphicText(
          "Calculator",
          style: NeumorphicStyle(
            color: NeumorphicTheme.isUsingDark(context)
                ? Colors.white
                : NeumorphicThemeData.dark().baseColor,
            shape: NeumorphicShape.flat,
            depth: 1,
            intensity: 1,
            border: NeumorphicBorder(width: 1),
          ),
          textStyle:
              NeumorphicTextStyle(fontSize: 32, fontWeight: FontWeight.bold),
        ),
        leading: Neumorphic(
            style: NeumorphicStyle(
                shape: NeumorphicShape.flat,
                boxShape: NeumorphicBoxShape.circle()),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Icon(
                Icons.calculate,
                color: NeumorphicTheme.isUsingDark(context)
                    ? Colors.white
                    : Colors.black,
              ),
            )),
        actions: [
          NeumorphicButton(
            onPressed: () {
              if (NeumorphicTheme.isUsingDark(context)) {
                NeumorphicTheme.of(context)!.themeMode = ThemeMode.light;
                setState(() {
                  themModeIcon = Icons.sunny;
                });
              } else {
                NeumorphicTheme.of(context)!.themeMode = ThemeMode.dark;
                setState(() {
                  themModeIcon = Icons.dark_mode;
                });
              }
            },
            style: NeumorphicStyle(
                shape: NeumorphicShape.flat,
                boxShape: NeumorphicBoxShape.circle()),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Icon(
                themModeIcon,
                color: NeumorphicTheme.isUsingDark(context)
                    ? Colors.white
                    : Colors.black,
              ),
            ),
          )
        ],
      ),
      backgroundColor: NeumorphicTheme.baseColor(context),
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 10),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Neumorphic(
                style: NeumorphicStyle(
                  depth: -10,
                  intensity: 0.8,
                  border: NeumorphicBorder(
                      color: NeumorphicTheme.baseColor(context), width: 1),
                  boxShape:
                      NeumorphicBoxShape.roundRect(BorderRadius.circular(10)),
                ),
                child: Stack(children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 3.5,
                    padding: EdgeInsets.all(25),
                    child: mathExpressionCreator(),
                  ),
                  Positioned(
                    child: resultCreator(),
                    right: 25,
                    bottom: 25,
                  )
                ])),
            buttons()
          ],
        ),
      ),
    );
  }
}
