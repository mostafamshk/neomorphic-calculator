import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:neomorphismapp/pages/home/home.dart';

void main() {
  runApp(MyApp());
}
//hello i am mostafa
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const NeumorphicApp(
      debugShowCheckedModeBanner: false,
      title: 'Neumorphism Design',
      themeMode: ThemeMode.light,
      theme: NeumorphicThemeData(
        baseColor: Colors.white,
        accentColor: Color.fromARGB(255, 255, 68, 68),
        lightSource: LightSource.topLeft,
        depth: 10,
        intensity: 1,
      ),
      darkTheme: NeumorphicThemeData(
          baseColor: Color.fromARGB(255, 54, 54, 54),
          accentColor: Colors.orange,
          lightSource: LightSource.topLeft,
          depth: 5,
          intensity: 0.5),
      home: Calculator(),
    );
  }
}
